FROM linuxserver/calibre
RUN apt update && apt upgrade -y && apt install -y tar libpng-dev libjpeg-dev && rm -rf /var/lib/apt/lists/*
COPY kindle /bin/kindle
ENTRYPOINT ["/bin/kindle"]
