/*
 * Copyright (C) 2023, Skores Media.
 * All rights reserved.
 */

package sse

import (
	"errors"
	"net/http"
	"strconv"
	"time"
)

func NewServer(w http.ResponseWriter, r *http.Request) *Server {
	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Transfer-Encoding", "chunked")

	srv := &Server{
		writer:  w,
		request: r,
		ticker:  time.NewTicker(time.Second * 30),
	}

	_ = srv.WriteComment("connected")

	if srv.ticker != nil {
		go func() {
			for {
				select {
				case <-srv.request.Context().Done():
					srv.ticker.Stop()
					return
				case t := <-srv.ticker.C:
					_ = srv.WriteComment(strconv.FormatInt(t.UnixMilli(), 10))
				}
			}
		}()
	}

	return srv
}

type Server struct {
	writer  http.ResponseWriter
	request *http.Request
	ticker  *time.Ticker
}

func (sse *Server) Write(data []byte) error {
	_, err := sse.writer.Write(data)
	return err
}

func (sse *Server) Send(evt Event) error {
	flusher, ok := sse.writer.(http.Flusher)
	if !ok {
		return errors.New("SSE not supported")
	}

	data, err := evt.Encode()
	if err != nil {
		return err
	}

	if err = sse.Write(data); err != nil {
		return err
	}

	flusher.Flush()

	return nil
}

func (sse *Server) WriteComment(comment string) error {
	return sse.Send(Event{Comment: comment})
}

func (sse *Server) WriteEvents(ch <-chan Event) error {
	for evt := range ch {
		err := sse.Send(evt)
		if err != nil {
			return err
		}
	}

	return nil
}
