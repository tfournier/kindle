/*
 * Copyright (C) 2023, Skores Media.
 * All rights reserved.
 */

package sse

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
)

func NewEvent() *Event {
	return &Event{}
}

type Event struct {
	Comment string
	ID      string
	Event   string
	Retry   time.Duration
	Data    any
}

func (evt *Event) SetComment(comment string) *Event {
	evt.Comment = comment
	return evt
}

func (evt *Event) SetID(id string) *Event {
	evt.ID = id
	return evt
}

func (evt *Event) SetEvent(event string) *Event {
	evt.Event = event
	return evt
}

func (evt *Event) SetData(data any) *Event {
	evt.Data = data
	return evt
}

func (evt *Event) SetRetry(retry time.Duration) *Event {
	evt.Retry = retry
	return evt
}

func (evt Event) Encode() ([]byte, error) {
	buf := bytes.NewBuffer([]byte{})

	if evt.Comment != "" {
		buf.WriteString(fmt.Sprintf(": %s\n", evt.Comment))
	}

	if evt.ID != "" {
		buf.WriteString(fmt.Sprintf("id: %s\n", evt.ID))
	}

	if evt.Event != "" {
		buf.WriteString(fmt.Sprintf("event: %s\n", evt.Event))
	}

	if evt.Retry.Milliseconds() > 0 {
		buf.WriteString(fmt.Sprintf("retry: %d\n", evt.Retry.Milliseconds()))
	}

	if evt.Data != nil {
		data, err := encode(evt.Data)
		if err != nil {
			return nil, err
		}
		for _, str := range strings.Split(string(data), "\n") {
			buf.WriteString(fmt.Sprintf("data: %s\n", str))
		}
	}

	buf.WriteString("\n")

	return buf.Bytes(), nil
}

func encode(v any) ([]byte, error) {
	if v == nil {
		return nil, nil
	}

	vo := reflect.ValueOf(v)

	for vo.Kind() == reflect.Pointer {
		if vo.IsNil() {
			break
		}

		vo = vo.Elem()
	}

	switch vo.Kind() {
	case reflect.Bool:
		return []byte(strconv.FormatBool(vo.Bool())), nil
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return []byte(strconv.FormatInt(vo.Int(), 10)), nil
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return []byte(strconv.FormatUint(vo.Uint(), 10)), nil
	case reflect.Float32:
		return []byte(strconv.FormatFloat(vo.Float(), 'f', 10, 32)), nil
	case reflect.Float64:
		return []byte(strconv.FormatFloat(vo.Float(), 'f', 10, 64)), nil
	case reflect.Complex64:
		return []byte(strconv.FormatComplex(vo.Complex(), 'f', 10, 64)), nil
	case reflect.Complex128:
		return []byte(strconv.FormatComplex(vo.Complex(), 'f', 10, 128)), nil
	case reflect.Array, reflect.Map, reflect.Slice, reflect.Struct:
		return json.Marshal(vo.Interface())
	case reflect.String:
		return []byte(vo.String()), nil
	default:
		return nil, fmt.Errorf("invalid data type (%s)", vo.Kind().String())
	}
}
