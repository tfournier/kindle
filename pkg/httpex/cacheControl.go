/*
 * Copyright (c) 2024, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package httpex

import (
	"gopkg.org/header"
	"net/http"
)

func CacheControl(value string, h http.Handler) http.Handler {
	if value == "" {
		return h
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(header.CacheControl, value)
		h.ServeHTTP(w, r)
	})
}
