.PHONY: build
build:
	goreleaser release --clean --snapshot

.PHONY: release
release:
	goreleaser release --clean
