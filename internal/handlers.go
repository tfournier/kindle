/*
 * Copyright (c) 2024, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package internal

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/tfournier/kindle/pkg/sse"
	"gopkg.org/header"
	"gopkg.org/mime"
)

func BooksHandler(c *Client) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Accept") == "text/event-stream" {
			srv := sse.NewServer(w, r)
			for book := range c.Stream(r.Context()) {
				if err := srv.Send(sse.Event{Data: book}); err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
			}
			return
		} else {
			//books, err := c.List()
			//if err != nil {
			//	http.Error(w, fmt.Errorf("BooksList => %w", err).Error(), http.StatusInternalServerError)
			//	return
			//}
			data, err := json.Marshal(c.List())
			if err != nil {
				http.Error(w, fmt.Errorf("JSON => %w", err).Error(), http.StatusInternalServerError)
				return
			}
			w.Header().Set(header.ContentType, mime.ApplicationJSON)
			_, _ = w.Write(data)
		}
	})
}

func DownloadHandler(c *Client) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		data, contentType, err := c.File(r.URL.Path)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set(header.ContentType, contentType)
		_, _ = w.Write(data)
	})
}
