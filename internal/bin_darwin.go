/*
 * Copyright (c) 2024, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package internal

const bin = "/Applications/calibre.app/Contents/MacOS/ebook-convert"
