/*
 * Copyright (c) 2024, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package internal

type Book struct {
	Path        string   `json:"path"`
	Title       string   `json:"title"`
	Description string   `json:"description"`
	Subjects    []string `json:"subjects"`
	Authors     []string `json:"authors"`
	Publisher   string   `json:"publisher"`
	Published   string   `json:"published"`
	Language    string   `json:"language"`
	ISBN        string   `json:"ISBN"`
	ASIN        string   `json:"ASIN"`
	Cover       string   `json:"cover"`
	Thumbnail   string   `json:"thumbnail"`
	TS          int64    `json:"ts"`
}
