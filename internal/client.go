/*
 * Copyright (c) 2024, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package internal

import (
	"context"
	"errors"
	"io"
	"mime"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"time"
	"unicode"

	"github.com/efskap/mobi"
	"github.com/radovskyb/watcher"
	"github.com/sirupsen/logrus"
	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
	"gopkg.org/random"
)

func New(root string) (*Client, error) {
	c := &Client{
		root:      root,
		withCover: false,
		watcher:   watcher.New(),
		books:     make([]Book, 0),
		channels:  make(map[string]chan Book),
		mutex:     new(sync.RWMutex),
	}

	c.watcher.FilterOps(watcher.Create)
	c.watcher.AddFilterHook(watcher.RegexFilterHook(regexp.MustCompile(`(?mi).*\.(?:epub|mobi)$`), true))

	go func() {
		for {
			select {
			case event := <-c.watcher.Event:
				if event.Path == "-" {
					continue
				}
				if err := c.handle(event.Path); err != nil {
					logrus.WithField("src", event.Path).Error(err)
				}
			case err := <-c.watcher.Error:
				logrus.Error(err)
			case <-c.watcher.Closed:
				return
			}
		}
	}()

	if err := c.watcher.AddRecursive(root); err != nil {
		return nil, err
	}

	go func() {
		for path := range c.watcher.WatchedFiles() {
			if err := c.handle(path); err != nil {
				logrus.WithField("src", path).Error(err)
			}
		}

		c.watcher.Wait()
		c.watcher.TriggerEvent(watcher.Create, nil)

		if err := c.watcher.Start(time.Second); err != nil {
			logrus.Fatal(err)
		}
	}()

	return c, nil
}

type Client struct {
	root      string
	withCover bool
	watcher   *watcher.Watcher
	books     []Book
	channels  map[string]chan Book
	mutex     *sync.RWMutex
}

func (c *Client) Close() {
	c.watcher.Close()
}

func (c *Client) File(name string) ([]byte, string, error) {
	path := filepath.Join(c.root, name)

	data, err := os.ReadFile(path)
	if err != nil {
		return nil, "", err
	}
	contentType := mime.TypeByExtension(filepath.Ext(name))
	if contentType == "" {
		contentType = http.DetectContentType(data)
	}

	return data, contentType, nil
}

func (c *Client) List() []Book {
	c.mutex.RLock()
	defer c.mutex.RUnlock()
	return c.books
}

func (c *Client) Get(name string) (Book, error) {
	path := filepath.Join(c.root, name)

	if ext := filepath.Ext(name); ext != ".mobi" {
		return Book{}, errors.New("file is not .mobi")
	}

	stat, err := os.Stat(path)
	if err != nil {
		return Book{}, err
	}

	reader, err := mobi.NewReader(path)
	if err != nil {
		return Book{}, err
	}

	location, err := filepath.Rel(c.root, path)
	if err != nil {
		return Book{}, err
	}

	book := Book{
		Path:        location,
		Title:       reader.FullName(),
		Description: reader.Description(),
		Subjects:    reader.Subjects(),
		Authors:     reader.Authors(),
		Publisher:   reader.Publisher(),
		Published:   reader.PublishingDate(),
		Language:    reader.Language(),
		ISBN:        reader.Isbn(),
		ASIN:        reader.Asin(),
		TS:          stat.ModTime().UnixMilli(),
	}

	if reader.HasCover() && c.withCover {
		book.Cover, err = encodeImage(reader.Cover())
		if err != nil {
			return Book{}, err
		}
	}

	if reader.HasThumbnail() {
		book.Thumbnail, err = encodeImage(reader.Thumbnail())
		if err != nil {
			return Book{}, err
		}
	}

	if err = reader.Close(); err != nil && err != io.EOF {
		return Book{}, err
	}

	return book, nil
}

func (c *Client) Stream(ctx context.Context) <-chan Book {
	ch := make(chan Book)

	id := random.Hexadecimal.Generate(8)

	go func() {
		for _, book := range c.List() {
			ch <- book
		}
		for {
			select {
			case <-ctx.Done():
				c.mutex.Lock()
				if _, ok := c.channels[id]; ok {
					if c.channels[id] != nil {
						close(c.channels[id])
					}
					delete(c.channels, id)
				}
				c.mutex.Unlock()
				return
			}
		}
	}()

	c.mutex.Lock()
	c.channels[id] = ch
	c.mutex.Unlock()

	return ch
}

func (c *Client) handle(path string) error {
	dst, err := filepath.Rel(c.root, path)
	if err != nil {
		return err
	}
	switch strings.ToLower(filepath.Ext(dst)) {
	case ".mobi":
		book, err := c.Get(dst)
		if err != nil {
			return err
		}
		c.mutex.RLock()
		c.books = append(c.books, book)
		for _, ch := range c.channels {
			ch <- book
		}
		c.mutex.RUnlock()
	case ".epub":
		return c.convert(dst)
	}
	return nil
}

func (c *Client) convert(name string) error {
	if strings.ToLower(filepath.Ext(name)) != ".epub" {
		return nil
	}
	src := filepath.Join(c.root, name)
	t := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	output, _, err := transform.String(t, name)
	if err != nil {
		return err
	}
	dst := filepath.Join(filepath.Dir(src), strings.TrimSuffix(filepath.Base(output), filepath.Ext(output))) + ".mobi"
	if stat, err := os.Stat(dst); !os.IsNotExist(err) && (stat.Size() > 0 || stat.IsDir()) {
		return nil
	}
	cmd := exec.Command(bin, src, dst, "--share-not-sync")
	cmd.Stdout = logrus.StandardLogger().WithField("src", name).WriterLevel(logrus.TraceLevel)
	cmd.Stderr = logrus.StandardLogger().WithField("src", name).WriterLevel(logrus.WarnLevel)
	return cmd.Run()
}
