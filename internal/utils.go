/*
 * Copyright (c) 2024, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package internal

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"image"
	"image/jpeg"
)

func encodeImage(img image.Image, err error) (string, error) {
	if err != nil {
		return "", err
	}

	if img == nil {
		return "", nil
	}

	var buffer bytes.Buffer

	if err = jpeg.Encode(&buffer, img, &jpeg.Options{Quality: jpeg.DefaultQuality}); err != nil {
		return "", err
	}

	b64 := base64.StdEncoding.EncodeToString(buffer.Bytes())

	return fmt.Sprintf("data:%s;base64,%s", "image/jpeg", b64), nil
}
