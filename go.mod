module gitlab.com/tfournier/kindle

go 1.21.5

require (
	github.com/efskap/mobi v0.0.0-20200528223731-40e34e50eb07
	github.com/radovskyb/watcher v1.0.7
	github.com/sirupsen/logrus v1.9.3
	github.com/urfave/cli/v2 v2.27.1
	golang.org/x/text v0.14.0
	gopkg.org/header v1.2.0
	gopkg.org/mime v1.1.0
	gopkg.org/random v1.0.1
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.3 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20231213231151-1d8dd44e695e // indirect
	golang.org/x/sys v0.5.0 // indirect
)
