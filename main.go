/*
 * Copyright (c) 2024, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package main

import (
	"embed"
	"gitlab.com/tfournier/kindle/pkg/httpex"
	"io/fs"
	"net/http"
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/tfournier/kindle/internal"
)

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"

	//go:embed all:public
	publicFS embed.FS
)

func init() {
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true})
}

func main() {
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:     "addr",
			Usage:    "Server listen address",
			EnvVars:  []string{"APP_ADDR"},
			Value:    ":3000",
			Required: false,
		},
		&cli.StringFlag{
			Name:     "source",
			Usage:    "Books source folder",
			EnvVars:  []string{"APP_SOURCE"},
			Value:    "./books/",
			Required: false,
		},
		&cli.BoolFlag{
			Name:     "debug",
			Usage:    "Enable debug mode",
			EnvVars:  []string{"APP_DEBUG"},
			Value:    false,
			Required: false,
		},
	}

	app := &cli.App{
		Name:      "kindle",
		Usage:     "Kindle Web Server",
		Flags:     flags,
		Before:    before,
		Action:    action,
		Version:   version,
		Authors:   []*cli.Author{{Name: "Thomas FOURNIER", Email: "tfournier@evolves.fr"}},
		Copyright: "Copyright (c) 2024, Thomas FOURNIER and contributors. All rights reserved.",
	}

	if err := app.Run(os.Args); err != nil {
		panic(err)
	}
}

func before(ctx *cli.Context) error {
	if ctx.Bool("debug") {
		logrus.SetLevel(logrus.TraceLevel)
	}

	source, err := filepath.Abs(ctx.String("source"))
	if err != nil {
		return err
	}

	if err = ctx.Set("source", source); err != nil {
		return err
	}

	return nil
}

func action(ctx *cli.Context) error {
	public, _ := fs.Sub(publicFS, "public")

	client, err := internal.New(ctx.String("source"))
	if err != nil {
		return err
	}
	defer client.Close()

	mux := http.NewServeMux()
	mux.Handle("/books.json", internal.BooksHandler(client))
	mux.Handle("/dl/", http.StripPrefix("/dl/", internal.DownloadHandler(client)))
	mux.Handle("/", httpex.CacheControl("max-age=604800", http.FileServer(http.FS(public))))

	logrus.Infof("Starting on %s", ctx.String("addr"))

	return http.ListenAndServe(ctx.String("addr"), mux)
}
